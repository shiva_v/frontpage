import Head from 'next/head'
import { logThisShit } from '../utils.js'

const styles = {
  appContainer: {
    margin: '10px'
  },
  appHeading: {
    color: 'cornflowerblue',
    paddingBottom: '10px',
    borderBottom: '1px solid cornflowerblue'
  }
}

const Index = () => (
  <div>
    <Head>
      <title>frontpage</title>
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.min.css" />
    </Head>
    <div className="container">
      <div className="app-container" style={styles.appContainer}>
        <div className="heading-container">
          <h1 class="title" style={styles.appHeading}>front.page</h1>
        </div>
      </div>
    </div>
  </div>
)

export default Index